
#include<stdio.h>
 
int Fibonacci(int);
 
int main()
{
   int n,x, i = 0;
    printf("Sequence needed upto ?");
   scanf("%d",&n);
 
   printf("Fibonacci series till %d calls\n",n);
 
   for ( x = 1 ; x <= n ; x++ )
   {
      printf("%d\n", Fibonacci(i));
      i++;
   }
 
   return 0;
}
 
int Fibonacci(int n)
{
   if ( n == 0 )
      return 0;
   else if ( n == 1 )
      return 1;
   else
      return ( Fibonacci(n-1) + Fibonacci(n-2) );
}