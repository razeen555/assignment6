//  Created by Razeen on 7/8/20.
//  Copyright © 2020 Razeen. All rights reserved.
//

 
 
#include<stdio.h>
void function(int n);

int main( )
{
 
    int x;
    printf("How many lines of output required ? ");
    scanf("%d",&x);
            
    function(x);
        printf("\n");
        return 0;
}
 
void function(int x)
{
        int i;
        if(x==0)
                return;
    else
        {
                function(x-1);
                for(i=x; i>=1; i--)
                        printf("%d ",i);
        printf("\n");
        }
}
